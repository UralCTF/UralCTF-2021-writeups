# Introduction to Networks 2

## Описание

> У провайдера опять что то не ладно, надеюсь это в последний раз..

## Решение

Задание решается по аналогии с Introduction to Networks 1.  
За искключением того, что на интерфейсах роутера необходимо задать IP адреса.

Для того, чтобы открыть прикрепленный файл понадобится утилита Cisco Packet Tracer, её можно легко нагуглить исходя из расширения файла.  
Открыв файл, видно несколько устройств: компьютер, роутер, сервер.  
Опытным путём можно выяснить, что единственное к чему мы имеем доступ - это консоль роутера и компьютер.  
Также на исходящих соединениях роутера видны красные треугольники - это выключенные интерфейсы.
Для того, чтобы восстановить соединение необходимо их включить. дальнейшие действия выполняются в консоли роутера:

1. Необходимо войти в привилегированный режим конфигурации с помощью команды `enable`.
2. Далее необходимо войти в режим конфигурации коммутатора командой `configure terminal`.
3. Затем следует выбрать нужный интерфейс (они подписаны рядом с соединениями `Gig 0/0/0` и `Gig 0/0/1`) командой `interface GigabitEthernet 0/0/0`.
4. Для того, чтобы включить интерфейс в режиме конфигурации интерфейса необходимо написать команду `no shutdown`.
5. Также, необходимо задать интерфейсу IP адрес командой `ip address`. В левой подсети, относящейся к компьютеру, задаваемый IP адрес должен быть из сети `192.168.0.0/24`. В правой подсети, относящейся к серверу, IP адрес интерфейса должен быть из сети `10.10.10.0/24`. (пример команды: `ip address 192.168.0.1 255.255.255.0`)
6. Затем нужно выйти из режима конфигурации интерфейса командой `exit`.
7. Проделав пункты 3-5 для оставшегося интерфейса, соединение между компьютером и сервером будет восстановлено.

Затем, необходимо на "рабочем столе" компьютера открыть браузер и зайти на сайт `uralctf.org`. На нём располагается флаг.
Флаг: `UralCTF{pr4153_70_7h3_r0u73r}`
