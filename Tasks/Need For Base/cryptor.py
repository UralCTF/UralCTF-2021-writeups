from base64 import b32encode, b64encode
from time import sleep
from threading import Thread


def to_b32():
    global flag
    sleep(0.1)
    flag = b32encode(flag)
    sleep(0.1)
    return


def to_b64():
    global flag
    sleep(0.1)
    flag = b64encode(flag)
    sleep(0.1)
    return


if __name__ == "__main__":
    flag = < FLAG UNKNOWN>  # flag has been lost

    threadsX = [Thread(target=to_b32) for x in range(7)]
    threadsY = [Thread(target=to_b64) for x in range(7)]
    for thread in threadsX+threadsY:
        thread.start()
    for thread in threadsX+threadsY:
        thread.join()

    with open('res.txt', 'wb') as File:
        File.write(flag)
