# Тайное местечко

## Описание

Флаг зашифрован с помощью base64 и base32, т.к. мы создали потоки и обьявили time.sleep, то у нас возникло состояние гонки (race condition) и порядок применения функций неизвестен. Нужно просто узнать алфавит base32 и алфавит base64, таким образом мы узнаем какой функции на каком этапе применить для расшифровки. Таск легко решается с использованием онлайн-декрипторов base64/base32.

## Пример реализации Python

```Python
from base64 import b32decode, b64decode
from string import ascii_lowercase

with open('res.txt', 'rb') as F:
    cipher = F.read()

while 1:
    if b'UralCTF' in cipher:
        print(cipher.decode())
        break
    elif any(map(lambda x: x in ascii_lowercase, cipher.decode())):
        cipher = b64decode(cipher)
    else:
        cipher = b32decode(cipher)

```
