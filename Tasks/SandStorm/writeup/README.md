# SandStorm

## Описание

> Ты тоже слышишь эту музыку?

## Решение

Получаем изображение с расширением jpg

Используя binwalk видим, что к изображению "приклеен" архив, достаём его.
Видим в архиве Sandstorm.mid, но архив запаролен.
Используем steghide с кодовой фразой `Darude`(Исполнитель на изображении) и получаем файл password.txt, содержимое которого: `dududududu`.

Разархивируем файл, используя пароль. В конце файла слышим странные звуки.

```clear
Файл с расширением .MID или .midi представляет собой файл цифрового интерфейса музыкальных инструментов. 
Файл MID может объяснить, какие ноты воспроизводятся, когда они воспроизводятся, и какой длины или громкости должна быть каждая нота.
```

Заглядываем внутрь с помощью онлайн утилиты, или любой другой утилиты для просмотра .mid файлов и видим флаг.

## Флаг : `UralCTF{50n6_n4m3_pl3453}`
