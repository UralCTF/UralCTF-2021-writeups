# Тайное местечко

Используем поиск по фото в любой поисковой системе, так мы узнаем что это фото сделано в Центральном Парке Нью-Йорка.
После этого заходим в maps.google и открываем streetview (либо используем аналогичные сервисы), ищем это место там и ракурс
с которого был сделан снимок, берем координаты в адресной строке, округляем как написано в задании, оборачиваем в UralCTF{}.

Done!

[Google maps](https://www.google.com/maps/@40.7790217,-73.9668144,3a,75y,136.98h,88.2t/data=!3m7!1e1!3m5!1s6MBBjuahXRYoCAer1QtFMA!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3D6MBBjuahXRYoCAer1QtFMA%26cb_client%3Dmaps_sv.tactile.gps%26w%3D203%26h%3D100%26yaw%3D172.81062%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656)
