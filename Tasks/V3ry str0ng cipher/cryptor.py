def crypt(flag: list):
    for i, item in enumerate(flag):
        flag[i] = str(polynomial(ord(item)))
    return ','.join(flag)


def polynomial(x):
    return 2 * x * x - 10 * x + 8


if __name__ == '__main__':
    flag = < FLAG UNKNOWN >
    with open('cipher', 'w') as File:
        File.write(crypt(list(flag)))
