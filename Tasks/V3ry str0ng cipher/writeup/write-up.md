# V3ry str0ng cipher

## Описание

Криптор берет ASCII-номер каждого символа флага, затем подставляет его как переменную в функцию (квадратное уравнение) и записывает ответ в файл (разделяя их между собой запятыми)

## Пример декриптора

```python
from math import sqrt


with open('cipher', 'r') as File:
    polynoms = File.read().split(',')
flag = ''
for polynom in polynoms:
    a, b, c = 2, -10, 8 - int(polynom)
    discr = b ** 2 - 4 * a * c
    if discr > 0:
        x1 = (-b + sqrt(discr)) / (2 * a)
        x2 = (-b - sqrt(discr)) / (2 * a)
        if x1 > 0:
            flag += chr(int(x1))
        elif x2 > 0:
            flag += chr(int(x2))
    elif discr == 0:
        x = -b / (2 * a)
        if x > 0:
            flag += chr(int(x))

print(flag)

```
