# The Lord of the Logs

## Описание

> Куда-то зашел, а куда - забыл...
> Что-то создал, а что - забыл...

## Решение

Имеем снятые с `Windows 7` лог-файлы.

Флаг разделен на две части.

Первую часть можно получить, посмотрев историю браузера по пути `C:\Users\Oyster\AppData\Local\Microsoft\Windows\WebCache` в файле `WebCacheV01.dat`. В истории находим ссылку <https://pastebin.com/kUDEXrqL>. Видим хэш - base64, декодируем.

Получаем первую часть флага: `UralCTF{l065_4r3_my`

Вторую часть можно найти в двух файлах: папке Recent(`C:\Users\Oyster\AppData\Roaming\Microsoft\Windows\Recent`) - `_l1f3}.lnk` или при анализе лог-файла`C:\Users\Oyster\NTUSER.DAT` (ветка`Software\Microsoft\Windows\CurrentVersion\Explorer\RecentDocs\.txt`) - `_l1f3}.txt`

Получаем вторую часть флага: `_l1f3}`

### Флаг : `UralCTF{l065_4r3_my_l1f3}`
