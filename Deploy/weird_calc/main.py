#!/usr/bin/env python3
import readline  # noqa
import sys
from code import interact


def readfunc(prompt: str):
    s = input(prompt)
    for name in dir(__builtins__) + ['import', '__builtins__', 'FLAG']:
        if name in s and name not in ('_', 'exit', 'help'):
            print(f'Cannot evaluate because prohibited word {name!r} found in input string {s!r}')
            return ''
    return s


stderr = sys.stderr
sys.stderr = sys.stdout
interact(
    banner=f'Calculator {sys.version}. Type exit() to exit.', exitmsg='', local={}, readfunc=readfunc
)
sys.stderr = stderr
