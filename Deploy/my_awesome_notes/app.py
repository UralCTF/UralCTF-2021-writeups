import hmac
import json
import logging
from base64 import urlsafe_b64encode, urlsafe_b64decode
from functools import wraps
from typing import AnyStr, Optional, TYPE_CHECKING
from uuid import uuid4
from smtplib import SMTP_SSL
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from flask import Flask, render_template, redirect, url_for, session, request, flash
from password_strength import PasswordPolicy

if TYPE_CHECKING:
    from . import db
else:
    import db

app = Flask(__name__)
app.secret_key = str(uuid4()).encode()

pass_checker = PasswordPolicy.from_names(
    length=8,
    uppercase=1,
    numbers=1,
    strength=0.55
)


def jwt_create(user_id: int) -> bytes:
    parts = (
        urlsafe_b64encode(json.dumps(dict(alg='hs256', typ='JWT')).encode()).rstrip(b'='),
        urlsafe_b64encode(json.dumps(dict(user_id=user_id)).encode()).rstrip(b'='),
    )
    unsigned_data = b'.'.join(parts)
    signature = urlsafe_b64encode(hmac.new(app.secret_key, unsigned_data,
                                           digestmod='sha256').digest()).rstrip(b'=')
    return b'.'.join((unsigned_data, signature))


def jwt_check(jwt: AnyStr) -> Optional[dict]:
    token = str(jwt)
    try:
        header_s, payload_s, sign = map(lambda s: s + ('=' * ((4 - len(s) % 4) % 4)),
                                        token.split('.', maxsplit=3))
    except ValueError:
        return None
    header = json.loads(urlsafe_b64decode(header_s))
    payload = json.loads(urlsafe_b64decode(payload_s))
    if not isinstance(header, dict):
        return None
    if not isinstance(payload, dict):
        return None
    if (alg := header.get('alg', '').lower()) == 'hs256':
        sign_check = urlsafe_b64encode(
            hmac.new(app.secret_key,
                     '.'.join((header_s.rstrip('='), payload_s.rstrip('='))).encode(),
                     digestmod='sha256').digest()
        ).decode()
        if sign != sign_check:
            return None
    elif alg != 'none':
        return None
    return payload


def send_recovery_mail(to: str, user) -> None:
    with SMTP_SSL("smtp.yandex.ru") as smtp:
        smtp.login("no-reply@utmn.city", "Fv25F6KyHudrTDX")
        msg = MIMEMultipart("alternative")
        msg["Subject"] = "Password recovery | My Awesome™ Notes"
        msg["From"] = '"My Awesome Notes" <no-reply@utmn.city>'
        msg["To"] = to
        token = jwt_create(user['id'])
        msg.attach(MIMEText(render_template('email.txt', token=token, user=user), "plain"))
        msg.attach(MIMEText(render_template('email.html', token=token, user=user), "html"))
        smtp.sendmail(msg["From"], [msg["To"]], msg.as_string())


def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        if not session.get('user_id'):
            flash("You must have been registered to view this page")
            return redirect(url_for('login'))
        user = db.find_user(id_=session.get('user_id'))
        if user is None:
            return redirect(url_for('logout'))
        return f(*args, **kwargs)
    return wrapper


@app.errorhandler(500)
def error(e):
    return render_template('500.html', code=e.code, name=e.name), 500


@app.errorhandler(404)
def not_found(_):
    return render_template('404.html'), 404


@app.route('/')
@login_required
def index():
    return redirect(url_for('notes'))


@app.route('/addNote')
@login_required
def add_note():
    return render_template('add_note.html')


@app.route('/addNote', methods=['POST'])
@login_required
def add_note_post():
    data = request.form
    title = data.get('title')
    text = data.get('text')
    is_private = bool(data.get('secret'))
    if not title:
        flash('Title is empty')
        return redirect(url_for('add_note'))
    if not text:
        flash('Text is empty')
        return redirect(url_for('add_note'))
    db.add_note(title, text, session.get('user_id'), is_private)
    return redirect(url_for('notes'))


@app.route('/notes')
@app.route('/notes/<int:user_id>')
@login_required
def notes(user_id: int = None):
    current_user_id = session.get('user_id')
    if user_id is None:
        return redirect(url_for('notes', user_id=current_user_id))
    user = db.find_user(id_=user_id)
    if user is None:
        flash('User not found')
        return redirect(url_for('notes'))
    return render_template('notes.html', notes=db.get_notes(user_id),
                           username=user['username'] if current_user_id != user['id'] else None)


@app.route('/login')
def login():
    if session.get('user_id'):
        return redirect(url_for('index'))
    if session.get('email'):
        session.pop('email')
    return render_template('login.html')


@app.route('/login', methods=['POST'])
def login_post():
    if session.get('user_id'):
        return redirect(url_for('index'))
    data = request.form
    username = data.get('username')
    password = data.get('password')
    if not username:
        flash('Username is empty')
        return redirect(url_for('login'))
    if not password:
        flash('Password is empty')
        return redirect(url_for('login'))
    user = db.find_user(username=username)
    if user is None:
        flash('User not found')
        return redirect(url_for('login'))
    if not db.check_pw(password, user['password']):
        session['username'] = username
        flash('Wrong password')
        return redirect(url_for('login'))
    session['user_id'] = user['id']
    session['username'] = user['username']
    return redirect(url_for('index'))


@app.route('/logout')
def logout():
    if session.get('user_id'):
        session.pop('user_id')
        session.pop('username')
    return redirect(url_for('login'))


@app.route('/recover')
def forgot_password_form():
    return render_template('recover.html')


@app.route('/recover', methods=['POST'])
def forgot_password_post():
    username = request.form.get('username')
    if not username:
        flash('Username is empty')
        return redirect(url_for('forgot_password_form'))
    user = db.find_user(username=username)
    if user is None:
        flash('User not found')
        return redirect(url_for('forgot_password_form'))
    email = user['email']
    try:
        send_recovery_mail(email, user)
    except Exception as e:
        flash(f'Failed to send email to {email}')
        logging.exception("Failed to send email to %s", email, exc_info=e)
        return redirect(url_for('forgot_password_form'))
    return render_template('recover_ok.html', email=email)


@app.route('/recover/<token>')
def actual_recover(token: str):
    if (data := jwt_check(token)) is None\
            or (user_id := data.get('user_id')) is None\
            or (user := db.find_user(id_=user_id)) is None:
        flash('Unknown error, try again')
        return redirect(url_for('forgot_password_form'))
    return render_template('change_password.html', user=user)


@app.route('/recover/<token>', methods=['POST'])
def actual_recover_post(token: str):
    if (data := jwt_check(token)) is None\
            or (user_id := data.get('user_id')) is None\
            or db.find_user(id_=user_id) is None:
        flash('Unknown error, try again')
        return redirect(url_for('forgot_password_form'))
    password = request.form.get('password')
    if not password:
        flash('Password is empty')
        return redirect(url_for('actual_recover', token=token))
    if pass_checker.test(password):
        flash('Password is not strong enough (make sure it has at least 8 chars including'
              ' uppercase and lowercase letters and digits)')
        return redirect(url_for('actual_recover', token=token))
    db.edit_password(user_id, password)
    session['username'] = db.find_user(id_=user_id)['username']
    return redirect(url_for('login'))


@app.route('/register')
def register():
    if session.get('user_id'):
        return redirect(url_for('index'))
    return render_template('register.html')


@app.route('/register', methods=['POST'])
def register_post():
    if session.get('user_id'):
        return redirect(url_for('index'))
    data = request.form
    email = data.get('email')
    username = data.get('username')
    password = data.get('password')
    if not email:
        flash('Email is empty')
        return redirect(url_for('register'))
    if not username:
        flash('Username is empty')
        return redirect(url_for('register'))
    if not password:
        flash('Password is empty')
        return redirect(url_for('register'))
    user = db.find_user(email=email)
    if user is not None:
        flash('Email already registered')
        return redirect(url_for('register'))
    session['email'] = email
    user = db.find_user(username=username)
    if user is not None:
        flash('Username already registered')
        return redirect(url_for('register'))
    session['username'] = username
    #if pass_checker.test(password):
    #    flash('Password is not strong enough (make sure it has at least 8 chars including'
     #         ' uppercase and lowercase letters and digits)')
     #   return redirect(url_for('register'))
    db.new_user(email, username, password)
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run(debug=True)
