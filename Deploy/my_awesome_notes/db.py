from hashlib import sha256
from os.path import exists
from sqlite3 import connect, Row
from logging import info


def _get_db():
    r = connect('app.db').cursor()
    r.row_factory = Row
    return r


if exists('init.sql') and not exists('app.db'):
    info('Database initialized')
    with open('init.sql') as f:
        _db = _get_db()
        _db.executescript(f.read())
        _db.connection.commit()
    del _db


def hash_pw(pw: str):
    return sha256(pw.encode()).hexdigest()


def check_pw(pw: str, hashed_pw: str):
    return hash_pw(pw) == hashed_pw


def get_notes(user_id: int):
    db = _get_db()
    db.execute('SELECT * FROM notes WHERE owner_id = ?', (user_id,))
    return db.fetchall()


def add_note(title: str, text: str, owner_id: int, is_private: bool = False):
    db = _get_db()
    db.execute('INSERT INTO notes (title, text, owner_id, is_private) VALUES (?, ?, ?, ?)',
               (title, text, owner_id, int(is_private)))
    db.connection.commit()


def find_user(*, username: str = None, email: str = None, id_: int = None):
    db = _get_db()
    if id_ is not None:
        db.execute('SELECT * FROM users WHERE id = ?', (id_,))
    elif email is not None:
        db.execute('SELECT * FROM users WHERE email = ?', (email,))
    else:
        db.execute('SELECT * FROM users WHERE username = ?', (username,))
    res = db.fetchall()
    return res[0] if res else None


def edit_password(user_id: int, new_password: str):
    db = _get_db()
    db.execute('UPDATE users SET password = ? WHERE id = ?', (hash_pw(new_password), user_id))
    db.connection.commit()


def new_user(email: str, username: str, password: str):
    db = _get_db()
    db.execute('INSERT INTO users (email, username, password) VALUES (?, ?, ?)',
               (email, username, hash_pw(password)))
    db.connection.commit()
