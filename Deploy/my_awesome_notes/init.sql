CREATE TABLE users
(
    id       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    email    TEXT    NOT NULL,
    username TEXT    NOT NULL,
    password TEXT    NOT NULL
);

INSERT INTO users (id, email, username, password) VALUES (1, 'foo@example.com', 'admin', '0000000000000000000000000000000000000000000000000000000000000000'); /* I don't think this hash exists */

CREATE TABLE notes
(
    id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    title      TEXT    NOT NULL,
    text       TEXT    NOT NULL,
    owner_id   INTEGER NOT NULL REFERENCES users(id),
    is_private INTEGER NOT NULL,
    CHECK (is_private IN (0, 1))
);

INSERT INTO notes (id, title, text, owner_id, is_private)
VALUES (1, 'My first note | FAQ', 'Hello, World!
This is my first note. I like to play CTF. I wrote this service to show you how to exchange notes and ideas securely and safely. This website is hosted on my private server, so don''t try to hack it, please. If you like my website, share it with others to make it more popular. Thank you!
Best wishes,
admin

P.S. Here is your promo-code for being one of the first visitors of my website: UralCTF{FIRSTVISITOR1010_2VyX2l}.', 1, 0),
       (2, 'My day', 'Today is a beautiful day! I woke up at 6:30, made a breakfast, then drove to work. My boss promoted me and I was very happy to hear it. Now I''m senior Pomidor programmer! YAY!!!', 1, 0),
       (3, 'Test secret note', 'This is a secret note only for me. You can''t have been reading this if you''re not admin. If you can read it, contact me through my email (foo@example.com). Thank you.', 1, 1),
       (4, 'CTF flags', 'I''m going to collect here every CTF flag I''ve ever found.

CupCTF{l4unch_c0d3s_st4rt}
cybrics{fe414125cafedeadeeb0052}
vka{r0b07_0r_n07_r0b07_7h47_15_7h3_qu35710n}
Cup{Ar3Y0uR34dyB4by?}
CTF{CnDtrAJSncN}
mctf{0h_it_ha5_f1n4lly_c0mpu73d}
unictf{voice_in_a_glitch}
UralCTF{h4v3_u_3vr_b33n_h4ck3d_by_any1??_GciOiA}', 1, 1),
       (6, 'Hacker''s Manifesto', 'Another one got caught today, it''s all over the papers.  "Teenager
Arrested in Computer Crime Scandal", "Hacker Arrested after Bank Tampering"...
        Damn kids.  They''re all alike.

        But did you, in your three-piece psychology and 1950''s technobrain,
ever take a look behind the eyes of the hacker?  Did you ever wonder what
made him tick, what forces shaped him, what may have molded him?
        I am a hacker, enter my world...
        Mine is a world that begins with school... I''m smarter than most of
the other kids, this crap they teach us bores me...
        Damn underachiever.  They''re all alike.

        I''m in junior high or high school.  I''ve listened to teachers explain
for the fifteenth time how to reduce a fraction.  I understand it.  "No, Ms.
Smith, I didn''t show my work.  I did it in my head..."
        Damn kid.  Probably copied it.  They''re all alike.

        I made a discovery today.  I found a computer.  Wait a second, this is
cool.  It does what I want it to.  If it makes a mistake, it''s because I
screwed it up.  Not because it doesn''t like me...
                Or feels threatened by me...
                Or thinks I''m a smart ass...
                Or doesn''t like teaching and shouldn''t be here...
        Damn kid.  All he does is play games.  They''re all alike.

        And then it happened... a door opened to a world... rushing through
the phone line like heroin through an addict''s veins, an electronic pulse is
sent out, a refuge from the day-to-day incompetencies is sought... a board is
found.
        "This is it... this is where I belong..."
        I know everyone here... even if I''ve never met them, never talked to
them, may never hear from them again... I know you all...
        Damn kid.  Tying up the phone line again.  They''re all alike...

        You bet your ass we''re all alike... we''ve been spoon-fed baby food at
school when we hungered for steak... the bits of meat that you did let slip
through were pre-chewed and tasteless.  We''ve been dominated by sadists, or
ignored by the apathetic.  The few that had something to teach found us will-
ing pupils, but those few are like drops of water in the desert.

        This is our world now... the world of the electron and the switch, the
beauty of the baud.  We make use of a service already existing without paying
for what could be dirt-cheap if it wasn''t run by profiteering gluttons, and
you call us criminals.  We explore... and you call us criminals.  We seek
after knowledge... and you call us criminals.  We exist without skin color,
without nationality, without religious bias... and you call us criminals.
You build atomic bombs, you wage wars, you murder, cheat, and lie to us
and try to make us believe it''s for our own good, yet we''re the criminals.

        Yes, I am a criminal.  My crime is that of curiosity.  My crime is
that of judging people by what they say and think, not what they look like.
My crime is that of outsmarting you, something that you will never forgive me
for.

        I am a hacker, and this is my manifesto.  You may stop this individual,
but you can''t stop us all... after all, we''re all alike.', 1, 0),
       (5, 'What is CTF?', 'In computer security, Capture the Flag (CTF), a type of wargame, is a computer security competition. CTF contests are usually designed to serve as an educational exercise to give participants experience in securing a machine, as well as conducting and reacting to the sort of attacks found in the real world (i.e., bug bounty programs in professional settings). Reverse-engineering, network sniffing, protocol analysis, system administration, programming, and cryptoanalysis are all skills which have been required by prior CTF contests at DEF CON. There are three main styles of capture the flag competitions: attack/defense, hardware challenges and Jeopardy!.

In an attack/defense style competition, each team is given a machine (or a small network) to defend on an isolated network. Teams are scored on both their success in defending their assigned machine(s) and on their success in attacking the other team''s machines. Depending on the nature of the particular CTF game, teams may either be attempting to take an opponent''s flag from their machine or teams may be attempting to plant their own flag on their opponent''s machine. Two of the more prominent attack/defense CTF''s are held every year at DEF CON, the largest hacker conference, and the NYU-CSAW (Cyber Security Awareness Week), the largest student cyber-security contest.

Hardware challenges usually involve getting an unknown piece of hardware and having to figure out how to bypass part of the security, e.g. using debugging ports or using a Side-channel attack.

Jeopardy!-style competitions usually involve multiple categories of problems, each of which contains a variety of questions of different point values and difficulties. Teams attempt to earn the most points in the competition''s time frame (for example 24 hours), but do not directly attack each other. Rather than a race, this style of game play encourages taking time to approach challenges and prioritizes quantity of correct submissions over the timing. 

Source: Wikipedia (https://en.wikipedia.org/wiki/Capture_the_flag#Computer_security)', 1, 0),
       (7, 'My favorite song', 'Zakryvayu dver'' kvartiry
Otklyuchayu vse mobily
Nedostupna dlya debilov
Potomu chto ya vlyubilas''
V tebya-a-a, tupogo nagleca
Ot chego zhe? Ot chego zhe?

Vsyo potomu, chto Dora — dura
Superdura, Dora — dura
Potomu chto Dora — dura
Superdura, Dora — dura
Potomu chto Dora — dura
Superdura, Dora — dura
Potomu chto Dora — dura', 1, 0),
       (8, 'Curious note', 'Never did I think I''d write a message like this, so, I''m just
gonna ask you thi: why did you read this note? Does this
give you some feeling of satisfaction? Amusement? Or maybe
you feel like you''ve accomplished something. Strange, huh?
Up until now, this was nothing but a fun note, but it''s 
never just a "fun note". Did you think I was really just
gonna make notes and not leave an important note? Well,
let me tell you something. This note is super important.
You read something in this note and I didn''t let you
down. But what is the actual message? Curious? Maybe you''ll
never know. Maybe I don''t even know. What I do know is you''re
gonna eventually find out what the message is before I even
run out of things to say. Did you figure it out yet? Look
around. Take a good look at this note. Analyze each word
and it''ll become clear. Imagine the ocean. Now imagine the
desert. Now imagine reading the first word on each line.
You just sang what you just read. Okay, have a nice day!', 1, 0);