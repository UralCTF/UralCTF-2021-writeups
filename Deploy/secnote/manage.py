#!/usr/bin/env python3
import os
import sys
from base64 import urlsafe_b64encode
from datetime import datetime
from os import urandom

from app import create_app


def main():
    app = create_app()
    app.run()


if __name__ == "__main__":
    main()
