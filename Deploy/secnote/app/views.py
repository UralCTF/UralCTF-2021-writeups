from datetime import datetime

from flask import current_app as app
from flask import request
from flask.helpers import flash, url_for
from flask.templating import render_template, render_template_string
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.urls import url_parse
from werkzeug.utils import redirect

from app import db, login
from app.forms import LoginForm, NoteForm, RegisterForm
from app.models import Note, User

login.login_view = 'login'
login.login_message = "Войдите, чтобы увидеть содержимое этой страницы"


@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route('/login', methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Неправильное имя пользователя или пароль',
                  category="alert-warning")
            return render_template('login.html', form=form)
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegisterForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            flash('Такой пользователь уже есть')
            return redirect(url_for('register'))
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("Пользователь создан! Теперь войдите в свой новый аккаунт.",
              category="alert-success")
        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    flash("Вы успешно вышли", "alert-success")
    return redirect(url_for('index'))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/notes/')
@app.route('/notes/<int:note_id>')
@login_required
def notes(note_id=None):
    if note_id:
        note = Note.query.get(note_id)
        if note:
            return render_template('note.html', note=note)
    notes = current_user.notes.all()
    return render_template('notes.html', notes=notes)


@app.route('/new', methods=["GET", "POST"])
@login_required
def new_note():
    form = NoteForm()
    if form.validate_on_submit():
        note = Note(title=form.title.data, content=form.content.data,
                    user_id=current_user.id, timestamp=datetime.now())
        if not "{" in note.content and not "}" in note.content:
            db.session.add(note)
            db.session.commit()
        note = {"title": note.title,
                "content": render_template_string(note.content)}
        flash(note, category="new-note-success")
    return render_template('new.html', form=form)
