from flask_wtf import FlaskForm
from wtforms import (BooleanField, PasswordField, StringField, SubmitField,
                     TextAreaField)
from wtforms.validators import DataRequired, Length


class LoginForm(FlaskForm):
    username = StringField('Имя пользователя', validators=[
                           DataRequired(), Length(max=25)])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')


class RegisterForm(FlaskForm):
    username = StringField('Имя пользователя', validators=[
                           DataRequired(), Length(max=25)])
    password = PasswordField('Пароль', validators=[
                             DataRequired(), Length(max=40)])
    submit = SubmitField("Зарегистрироваться")


class NoteForm(FlaskForm):
    title = StringField("Заголовок", validators=[
                        DataRequired(), Length(max=100)])
    content = TextAreaField("Текст заметки", validators=[
                            DataRequired(), Length(max=2000)])
    submit = SubmitField("Сохранить")
