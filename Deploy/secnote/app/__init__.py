from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from base64 import urlsafe_b64encode
from os import urandom
from datetime import datetime


db = SQLAlchemy()
login = LoginManager()


def create_app():
    app = Flask(__name__)
    app.config.from_object('app.config.Config')
    # app.config.from_envvar('SQLALCHEMY_DATABASE_URI')
    # app.config.from_envvar('SECRET_KEY')
    # app.config.from_envvar('SQLALCHEMY_TRACK_MODIFICATIONS')
    # app.config.from_envvar('FLAG')
    # TODO: Задокерить
    # TODO: Сделать автоматическое создание флага в заметках
    db.init_app(app)
    login.init_app(app)

    with app.app_context():
        from . import views
        from .models import User, Note
        
        db.drop_all()
        db.create_all()

        ctfadmin = User(username="ctfadmin")
        password = urlsafe_b64encode(urandom(16)).decode()
        print("Password", password)
        ctfadmin.set_password(password)
        db.session.add(ctfadmin)
        db.session.commit()
        ctfadmin = User.query.get(1)

        flag = app.config["IDORFLAG"]
        note1 = Note(
            title="Перед соревнованиями",
            user_id=ctfadmin.id,
            content=f"1. Провести регистрацию.\n"
            + "2. Оповестить о начале участников.\n"
            + "3. Запилить сервисы",
            timestamp=datetime.now(),
        )
        note2 = Note(
            title="Сделать вечером...",
            user_id=ctfadmin.id,
            content="Поднять SecNote. Сделать так, чтобы ничего не падало.\n"
            + "Запостить текст:\n\n"
            + "Ежегодные соревнования по информационной безопасности для студентов "
            + "и школьников Уральского федерального округа и участников из других регионов. "
            + "Соревнования проводятся Тюменским государственным университетом при поддержке "
            + "Правительства Тюменской области. "
            + f"По любым вопросам обращаться к руководителям сообщества.{flag}",
            timestamp=datetime.now(),
        )
        db.session.add(note1)
        db.session.add(note2)
        
        db.session.commit()
        return app
