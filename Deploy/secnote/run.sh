#!/usr/bin/env bash
docker run --rm --name vulnapp -p 5000:8000 -d \
	-e SQLALCHEMY_DATABASE_URI=sqlite:///./app.db \
	-e SECRET_KEY=No00tEs-HaCkInG \
	-e SQLALCHEMY_TRACK_MODIFICATIONS=False \
	-e SSTIFLAG="UralCTF{T3mp1at3s_Lik3_Ev3ry0ne_L1KeS}" \
	-e IDORFLAG="UralCTF{ID0RS_aR3_SO_s1mple}" \
	vulnapp
